import java.net.URI;

import javax.ws.rs.JAXRS;
import javax.ws.rs.JAXRS.Configuration;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.UriBuilder;

public class HelloWorld {
	public static void main(String[] args) throws InterruptedException {
		Application application = new HelloWorldApplication();

		Configuration configuration = Configuration.builder().protocol("http").host("localhost").port(8080).build();

		JAXRS.start(application, configuration).thenAccept(instance -> {
			Runtime.getRuntime()
					.addShutdownHook(new Thread(() -> instance.stop()
							.thenAccept(stopResult -> System.out.printf("Stop result: %s [Native stop result: %s]%n",
									stopResult, stopResult.unwrap(Object.class)))));

			final Configuration actualConfigurarion = instance.configuration();
			final URI uri = UriBuilder.newInstance().scheme(actualConfigurarion.protocol().toLowerCase())
					.host(actualConfigurarion.host()).port(actualConfigurarion.port())
					.path(actualConfigurarion.rootPath()).build();

			System.out.printf("Listening to %s - Send SIGKILL to shutdown.%n", uri);
		});

		Thread.currentThread().join();
	}
}
